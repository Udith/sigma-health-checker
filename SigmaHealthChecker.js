const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const cloudFront = new AWS.CloudFront();
const cognitoISP = new AWS.CognitoIdentityServiceProvider();
const ddb = new AWS.DynamoDB();
const apig = new AWS.APIGateway();
const lambda = new AWS.Lambda();
const ses = new AWS.SES();

const MAX_RETRIES = 3;
const STATUS_EMAIL_FROM = "sigma-health@slappforge.com";
const STATUS_EMAIL_TO = process.env.STATUS_EMAIL_TO;
const DR_GUIDE = "https://docs.google.com/document/d/1eRyUcccfwnu9Jh6gWnccDuPqjJOcqTy7dn-1ZL9PcFk/edit?usp=sharing";

const SIGMA_BUCKET_NAME = "sigma.slappforge.com";
const SIGMA_CF_ID = "E23L6XL6245I99";
const SIGMA_USER_POOL = "us-east-1_fX9cShkMH";
const SIGMA_DDB_TABLES = [
    "AWSCouponUsage",
    "AWSSubscriptions",
    "DashboardWidgets",
    "SLAccessKeys",
    "SigmaAnalytics",
    "SigmaUserProjects",
    "StripeSubscriptions",
    "sigma_feedback",
    "sigma_referrals"
];
const SIGMA_APIS = [
    { id: "1l3mec59sj", name: "slappforgeAnalytics" },
    { id: "3r35ntptoe", name: "slappforgeIAMProxy" },
    { id: "6n3p78kx56", name: "AWSSubscriptionAPI" },
    { id: "7u6sp0pyk7", name: "StripeProxyAPI" },
    { id: "dmegi1vc65", name: "CouponCodeAPI" },
    { id: "fcr6gonfd4", name: "slappforgeNPM" },
    { id: "g1vcc2mhif", name: "slappforgeYouTrackProxy" },
    { id: "gcqf9lyeqe", name: "AccessKeyManagerAPI" },
    { id: "hwrjublxek", name: "SigmaProjects" },
    { id: "m3tcwxwsvd", name: "StripeEventsAPI" },
    { id: "moq5zdbfjg", name: "slappforgeSigmaFeedback" },
    { id: "o3bmueesn6", name: "slappforgeS3Proxy" },
    { id: "stw98g6ya2", name: "slappforgeProdVCSProxy" },
    { id: "u4dbig2hk5", name: "slappforgePayments" }
];

const SIGMA_LAMBDAS = [
    // Subscription handling
    "AWSSubscriptionInfo",
    "AWSSubscriptionEventHandler",
    "StripeEventHandler",
    "CouponCodeHandler",
    "AWSSubscriptionHandler",
    "SubscriptionChangeHandler",
    "StripeAPIProxy",
    "ExpiredAccountDowngrader",

    // Debug codes
    "AccessCodeManagerKeyGenerator",
    "AccessCodeManagerKeyListGetter",

    // Sigma backend
    "slappforgeGetUserSignUpTimestamp",
    "slappforgeCheckEmailExists",
    "slappforgeS3CreateBucket",
    "slappforgePaymentHandler",
    "slappforgeResourceSync",
    "SigmaAnalyticsretriever",
    "SigmaAnalyticspersister",

    // User pool events
    "slappforgeUserPoolEventProcessor",
    "slappforgeUserPoolEventProcessorTest",

    // Sigma Dash
    "TESTGetProjects",
    "TESTSaveProject",
    "TESTRemoveProject",
    "TESTwidgetsGetWidgets",
    "TESTwidgetsRemoveWidget",
    "TESTwidgetsAddWidget"
];

exports.handler = async (event) => {
    let allResults = {};
    let overallSuccess = true;

    ({ allResults, overallSuccess } = await addToResults('S3', allResults, overallSuccess, checkBucket));
    ({ allResults, overallSuccess } = await addToResults('CloudFront', allResults, overallSuccess, checkCFDistribution));
    ({ allResults, overallSuccess } = await addToResults('Cognito', allResults, overallSuccess, checkCognito));
    ({ allResults, overallSuccess } = await addToResults('DDB', allResults, overallSuccess, checkDDBs));
    ({ allResults, overallSuccess } = await addToResults('APIG', allResults, overallSuccess, checkAPIs));
    ({ allResults, overallSuccess } = await addToResults('Lambda', allResults, overallSuccess, checkLambdas));

    await sendStatusEmail(overallSuccess, allResults);
    return true;
};

async function addToResults(resourceType, allResults, overallSuccess, operation) {
    let { isSuccess, result } = await operation();
    overallSuccess = overallSuccess && isSuccess;
    allResults[resourceType] = result;
    return { allResults, overallSuccess };
}

async function checkBucket() {
    return await tryOperation(async (attempt) => {
        console.log(`[Attempt ${attempt}] Checking S3 bucket...`);
        try {
            await s3.getObject({
                Bucket: SIGMA_BUCKET_NAME,
                Key: "index.html"
            }).promise();
            return generateResult(true, {
                [SIGMA_BUCKET_NAME]: true
            });

        } catch (err) {
            console.log('Bucket check failed', err);
            return generateResult(false, {
                [SIGMA_BUCKET_NAME]: false
            });
        };
    });

}

async function checkCFDistribution() {
    return await tryOperation(async (attempt) => {
        console.log(`[Attempt ${attempt}] Checking CF Distribution...`);
        try {
            await cloudFront.getDistribution({
                Id: SIGMA_CF_ID
            }).promise();
            return generateResult(true, {
                [SIGMA_CF_ID]: true
            });

        } catch (err) {
            console.log('CloudFront distribution check failed', err);
            return generateResult(false, {
                [SIGMA_CF_ID]: false
            });
        };
    });

}

async function checkCognito() {
    return await tryOperation(async (attempt) => {
        console.log(`[Attempt ${attempt}] Checking Cognito Userpool...`);
        try {
            await cognitoISP.describeUserPool({
                UserPoolId: SIGMA_USER_POOL
            }).promise();
            return generateResult(true, {
                [SIGMA_USER_POOL]: true
            });

        } catch (err) {
            console.log('Cognito Userpool check failed', err);
            return generateResult(false, {
                [SIGMA_USER_POOL]: false
            });
        };
    });

}

async function checkDDBs() {
    return await tryOperation(async (attempt) => {
        console.log(`[Attempt ${attempt}] Checking DDB Tables...`);
        try {
            let data = await ddb.listTables({
                Limit: 20
            }).promise();

            let tableNames = data.TableNames;
            let results = {};
            let allExists = true;
            SIGMA_DDB_TABLES.forEach(table => {
                let tableExists = tableNames.includes(table);
                allExists = allExists && tableExists;
                results[table] = tableExists;
            });
            return generateResult(allExists, results);

        } catch (err) {
            console.log('DDB Tables check failed', err);
            let results = {};
            SIGMA_DDB_TABLES.forEach(table => {
                results[table] = false;
            });
            return generateResult(false, results);
        };
    });
}

async function checkAPIs() {
    return await tryOperation(async (attempt) => {
        console.log(`[Attempt ${attempt}] Checking API Gateways...`);
        try {
            let data = await apig.getRestApis({
                limit: 50
            }).promise();

            let apiIDs = data.items.map(api => api.id);
            let results = {};
            let allExists = true;
            SIGMA_APIS.forEach(api => {
                let { id, name } = api;
                let apiExists = apiIDs.includes(id);
                allExists = allExists && apiExists;
                results[`${name} (${id})`] = apiExists;
            });
            return generateResult(allExists, results);

        } catch (err) {
            console.log('API Gateways check failed', err);
            let results = {};
            SIGMA_APIS.forEach(api => {
                let { id, name } = api;
                results[`${name} (${id})`] = false;
            });
            return generateResult(false, results);
        };
    });
}

async function checkLambdas() {
    return await tryOperation(async (attempt) => {
        console.log(`[Attempt ${attempt}] Checking Lambda functions...`);
        try {
            let data = await lambda.listFunctions({
                MaxItems: 50
            }).promise();

            let lambdaNames = data.Functions.map(func => func.FunctionName);

            if (data.NextMarker) {
                data = await lambda.listFunctions({
                    MaxItems: 50,
                    Marker: data.NextMarker
                }).promise();
                lambdaNames = lambdaNames.concat(data.Functions.map(func => func.FunctionName));
            }
            
            let results = {};
            let allExists = true;
            SIGMA_LAMBDAS.forEach(lambda => {
                let lambdaExists = lambdaNames.includes(lambda);
                allExists = allExists && lambdaExists;
                results[lambda] = lambdaExists;
            });
            return generateResult(allExists, results);

        } catch (err) {
            console.log('Lambda functions check failed', err);
            let results = {};
            SIGMA_LAMBDAS.forEach(lambda => {
                results[lambda] = false;
            });
            return generateResult(false, results);
        };
    });
}

async function tryOperation(op) {
    let isSuccess = false;
    let result = {};

    let attempt = 1;
    while ((!isSuccess) && (attempt <= MAX_RETRIES)) {
        ({ isSuccess, result } = await op(attempt++));
    }
    return { isSuccess, result };
}

function generateResult(isSuccess, result) {
    return { isSuccess, result };
}

async function sendStatusEmail(overallSuccess, results) {
    let subject = overallSuccess ? '✅ Sigma Deployment Health Check - Passed' : '️‼️ Sigma Deployment Health Check - Failed';
    let body = '<table>';
    Object.keys(results).forEach(resourceType => {
        let resources = results[resourceType];
        Object.keys(resources).forEach(resource => {
            let status = resources[resource];
            body += '<tr>';
            body += `<td>[${resourceType}]</td>`;
            body += `<td>${resource}</td>`;
            body += `<td>${status ? '✅' : '❌'}</td>`;
            body += '</tr>';
        });
    });
    body += '</table>';

    body += '<h4>Disaster Recovery Guide</h4>';
    body += `<a href='${DR_GUIDE}'>${DR_GUIDE}</a>`

    try {
        await ses.sendEmail({
            Source: STATUS_EMAIL_FROM,
            Destination: {
                ToAddresses: [STATUS_EMAIL_TO]
            },
            Message: {
                Subject: {
                    Charset: "UTF-8",
                    Data: subject
                },
                Body: {
                    Html: {
                        Charset: "UTF-8",
                        Data: body
                    }
                }
            }
        }).promise();
        console.log("Successfully sent status email");
    } catch (err) {
        console.log("Failed to send status email", err);
    }
}